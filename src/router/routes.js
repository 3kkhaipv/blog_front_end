export default [
  {
    path: '/',
    name: 'HelloWorld',
    component: () => import('@/components/HelloWorld')
  },
  {
    path: '/admin',
    redirect: 'admin/home',
    component: () => import('@/layouts/Layout'),
    children: [
      {
        path: 'home',
        name: 'index-home',
        component: () => import('@/views/admin/home/IndexHome')
      },
      {
        path: 'users',
        name: 'index-user',
        component: () => import('@/views/admin/user/IndexUser')
      },
      {
        path: 'users/create',
        name: 'index-create',
        component: () => import('@/views/admin/user/CreateUser')
      },
      {
        path: 'categories',
        name: 'index-category',
        component: () => import('@/views/admin/category/IndexCategory')
      },
      {
        path: 'categories/create',
        name: 'create-category',
        component: () => import('@/views/admin/category/CreateCategory')
      },
      {
        path: 'categories/edit/:id',
        name: 'edit-category',
        component: () => import('@/views/admin/category/EditCategory')
      },
      {
        path: 'posts',
        name: 'index-post',
        component: () => import('@/views/admin/post/IndexPost')
      },
      {
        path: 'posts/create',
        name: 'create-post',
        component: () => import('@/views/admin/post/CreatePost')
      },
      {
        path: 'posts/edit/:id',
        name: 'edit-post',
        component: () => import('@/views/admin/post/EditPost')
      }
    ]
  },
  {
    path: '/login',
    name: 'admin-login',
    component: () => import('@/views/admin/Login')
  }
]
