import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'
import NProgress from 'nprogress'

Vue.use(Router)

const createRouter = new Router({
  mode: 'history',
  routes
})

createRouter.beforeEach((to, from, next) => {
  NProgress.start()
  if (to.name !== 'admin-login' && localStorage.getItem('token-admin') == null) next({ name: 'admin-login' })
  next()
})

createRouter.afterEach(() => {
  NProgress.done()
})

export default createRouter
