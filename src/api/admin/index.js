import callApi from '@/utils/call-api'
import request from '@/utils/request'

export function getApiListUser (params) {
  return callApi('admin/users', 'GET', '', params)
}

export function createApiUser (params) {
  return callApi('admin/users', 'POST', params, '')
}

export function deleteApiUser (id) {
  return callApi(`admin/users/${id}`, 'DELETE', null, '')
}

export function getApiListCategory (params) {
  return callApi('admin/categories', 'GET', '', params)
}

export function createApiCategory (params) {
  return callApi('admin/categories', 'POST', params, '')
}

export function deleteApiCategory (id) {
  return callApi(`admin/categories/${id}`, 'DELETE', null, '')
}

export function findApiCategory (id) {
  return callApi(`admin/categories/${id}`, 'GET', null, '')
}

export function updateApiCategory (id, params) {
  return callApi(`admin/categories/${id}`, 'PUT', params, '')
}

export function loginApi (params) {
  return callApi(`admin/login`, 'POST', params, '')
}

export function logoutApi (params) {
  return callApi(`admin/logout`, 'POST', params, '')
}

export function getApiListPost (params) {
  return callApi('admin/posts', 'GET', '', params)
}

export function createApiPost (params) {
  return request('admin/posts', params)
}

export function deleteApiPost (id) {
  return callApi(`admin/posts/${id}`, 'DELETE', null, '')
}

export function findApiPost (id) {
  return callApi(`admin/posts/${id}`, 'GET', null, '')
}

export function updateApiPost (id, params) {
  return request(`admin/posts/${id}`, params)
}
