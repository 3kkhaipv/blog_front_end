import axios from 'axios'

export default function request (endpoint, params) {
  const token = localStorage.getItem('token-admin')
  return axios.post(`${process.env.API_URL}/${endpoint}`,
    params,
    {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'multipart/form-data'
      }
    }
  ).then(function (response) {
    return response
  }).catch(function (error) {
    return error.response
  })
}
