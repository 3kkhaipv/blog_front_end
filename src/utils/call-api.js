import axios from 'axios'

export default function callApi (endpoint, method = 'GET', body, params) {
  const token = localStorage.getItem('token-admin')
  return axios({
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    method: `${method}`,
    url: `${process.env.API_URL}/${endpoint}`,
    data: body,
    params: params
  }).then(function (response) {
    return response
  }).catch(function (error) {
    return error.response
  })
}
