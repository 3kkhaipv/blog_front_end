import {
  getApiListUser,
  createApiUser,
  deleteApiUser,
  getApiListCategory,
  createApiCategory,
  deleteApiCategory,
  findApiCategory,
  updateApiCategory,
  loginApi,
  logoutApi,
  getApiListPost,
  deleteApiPost,
  createApiPost,
  findApiPost,
  updateApiPost
} from '@/api/admin/index'

const state = {
  user: {
    pagination: {},
    list: [],
    groups: []
  },
  findCategory: {},
  findPost: {},
  category: {
    pagination: {},
    list: [],
    groups: []
  },
  post: {
    pagination: {},
    list: [],
    groups: []
  }
}

const getters = {
  getListUser: (state) => {
    if (state.user.list.length !== 0) {
      return state.user.list
    }
    return []
  },
  getPaginationListUser: (state) => {
    if (state.user.pagination) {
      return state.user.pagination
    }
    return {}
  },
  // Category
  getListCategory: (state) => {
    if (state.category.list.length !== 0) {
      return state.category.list
    }
    return []
  },

  getFindCategory: state => {
    return state.findCategory
  },

  getPaginationListCategory: (state) => {
    if (state.category.pagination) {
      return state.category.pagination
    }
    return {}
  },
  getListPost: (state) => {
    if (state.post.list.length !== 0) {
      return state.post.list
    }
    return []
  },

  getFindPost: state => {
    return state.findPost
  },

  getPaginationListPost: (state) => {
    if (state.post.pagination) {
      return state.post.pagination
    }
    return {}
  }
}

const mutations = {
  SET_LIST_USER: (state, user) => {
    const {data, ...pagination} = user
    state.user = {pagination: pagination, list: data}
  },
  SET_LIST_CATEGORY: (state, category) => {
    const {data, ...pagination} = category
    state.category = {pagination: pagination, list: data}
  },
  SET_FIND_CATEGORY: (state, category) => {
    state.findCategory = category
  },
  SET_LIST_POST: (state, post) => {
    const {data, ...pagination} = post
    state.post = {pagination: pagination, list: data}
  },
  SET_FIND_POST: (state, post) => {
    state.findPost = post
  }
}

const actions = {
  async loginAction ({commit}, params) {
    try {
      return await loginApi(params)
    } catch (e) {
      return e
    }
  },
  async logoutAction ({commit}, params) {
    try {
      return await logoutApi(params)
    } catch (e) {
      return e
    }
  },
  async getListUserAction ({commit}, params) {
    try {
      const response = await getApiListUser(params)
      commit('SET_LIST_USER', response.data)
    } catch (e) {
      return e
    }
  },
  async createUserAction ({commit}, params) {
    try {
      await createApiUser(params)
      return 201
    } catch (e) {
      return e
    }
  },
  async destroyUserAction ({commit}, id) {
    try {
      await deleteApiUser(id)
      return true
    } catch (e) {
      return e
    }
  },
  async getListCategoryAction ({commit}, params) {
    try {
      const response = await getApiListCategory(params)
      commit('SET_LIST_CATEGORY', response.data)
    } catch (e) {
      return e
    }
  },
  async createCategoryAction ({commit}, params) {
    try {
      return await createApiCategory(params)
    } catch (e) {
      return e
    }
  },
  async destroyCategoryAction ({commit}, id) {
    try {
      return await deleteApiCategory(id)
    } catch (e) {
      return e
    }
  },
  async findCategoryAction ({commit}, id) {
    try {
      const response = await findApiCategory(id)
      commit('SET_FIND_CATEGORY', response.data)
    } catch (e) {
      return e
    }
  },
  async updateCategoryAction ({commit}, params) {
    try {
      return await updateApiCategory(params.id, params.values)
    } catch (e) {
      return e
    }
  },
  async getListPostAction ({commit}, params) {
    try {
      const response = await getApiListPost(params)
      commit('SET_LIST_POST', response.data)
    } catch (e) {
      return e
    }
  },
  async destroyPostAction ({commit}, id) {
    try {
      return await deleteApiPost(id)
    } catch (e) {
      return e
    }
  },
  async createPostAction ({commit}, params) {
    try {
      return await createApiPost(params)
    } catch (e) {
      return e
    }
  },
  async findPostAction ({commit}, id) {
    try {
      const response = await findApiPost(id)
      commit('SET_FIND_POST', response.data)
    } catch (e) {
      return e
    }
  },
  async updatePostAction ({commit}, params) {
    try {
      return await updateApiPost(params.id, params.values)
    } catch (e) {
      return e
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
